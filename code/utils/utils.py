def add_index_to_source(e):
    e['_source']['id']=e["_id"]
    return e['_source']

def response_to_source_list(response):
    return [add_index_to_source(e) for e in response['hits']['hits']]
