from flask import Flask
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search

from routes.index import create_route_index
from routes.persons import create_routes_persons

es = Elasticsearch(['es02'], scheme="http", port=9200)
searcher = Search(using=es)
app = Flask(__name__)

create_route_index(app)
create_routes_persons(app, es, searcher)

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
