from flask import jsonify
from utils.utils import response_to_source_list

class GetPersonsInRange:
    '''
    Clase que permite buscar personas en un rango de edad
    '''
    def __call__(self, es, min_age, max_age):

        response=es.search(body=
        {
            "query": {
                "range": {
                    "age": {
                        "gte": min_age,
                        "lte": max_age,
                        "boost": 2.0
                    }
                }
            }
        },index='personas')
        results=response_to_source_list(response)
        return jsonify(results)