from flask import jsonify
from utils.utils import response_to_source_list

class GetPersonsWithName:
    '''
    Clase que permite buscar personas cuyo nombre coincide con un string. 
    '''
    def __call__(self, searcher, name):

        response=searcher.filter('wildcard', **{"fullname": {"value": "*" + name + "*", "boost": 1.0, "rewrite": "constant_score"}}).execute()

        results=response_to_source_list(response.to_dict())
        return jsonify(results)
