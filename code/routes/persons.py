from dao.persons.get_persons_in_range import GetPersonsInRange
from dao.persons.get_persons_with_name import GetPersonsWithName

def create_routes_persons(app, es, searcher):

    @app.route('/personas/rango/<int:min_age>/<int:max_age>', methods=['GET'])
    def get_persons_in_range(min_age, max_age):
        get_persons_in_range = GetPersonsInRange()
        return get_persons_in_range(es, min_age, max_age)

    @app.route('/personas/nombre/<string:name>', methods=['GET'])
    def get_persons_with_name(name):
        get_persons_with_name = GetPersonsWithName()
        return get_persons_with_name(searcher, name)   