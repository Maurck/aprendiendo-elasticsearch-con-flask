# Pasos para levantar la aplicación

Construir la imagen flask_app
- docker-compose build

Levantar los servicios de elastic search y flask_app
- docker-compose up -d

# Pasos para creación del índice
Ejecutar lo siguiente:

Conexión al contenedor flask_app
- docker exec -it flask_app bash

Creación del índice personas
- curl -X PUT "http://es02:9200/personas/" -H "Content-Type: application/json" -d @code/data/index-mapping.json

Cargado de registros de las personas
- curl -X POST 'http://es02:9200/personas/_bulk?pretty' -H "Content-Type: application/x-ndjson" --data-binary @code/data/personas.json

# Pasos para probar las APIS
Ejecutar lo siguiente:

Conexión al contenedor flask_app
- docker exec -it flask_app bash

Obtencion de las personas con rango de edad 25 a 30
- curl -X GET 'http://localhost:5000/personas/rango/25/30' -H 'Content-Type: application/json'

Obtencion de personas cuyo nombre coincide con 'man'
- curl -X GET 'http://localhost:5000/personas/nombre/man' -H 'Content-Type: application/json'
